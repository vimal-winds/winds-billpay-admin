@extends('layouts.app')

@section('title', 'List Services')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{Session::get('success')}}
                    </div>
                @endif

                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{Session::get('error')}}
                    </div>
                @endif

                <h3>List Services</h3>

                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right">
                            <a href="{{ url('services/create') }}">
                                <button type="button" class="btn btn-primary">Add Service</button>
                            </a>
                        </div>
                    </div>
                </div>

                <table class="table table-bordered mt-3">
                    <thead>
                    <tr>
                        <th scope="col">Sl. No</th>
                        <th scope="col">Service ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Provider Alias</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Last Updated At</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $index => $service)
                    <tr>
                        <th scope="row">
                            {{ ($services->currentPage() > 0)
                            ? ((($services->currentPage()-1) * $services->perPage()) + $index + 1)
                            : ++$index }}</th>
                        <td>{{ $service->id }}</td>
                        <td>{{ $service->name }}</td>
                        <td>{{ $service->provider_alias }}</td>
                        <td>{{ $service->created_at->toDayDateTimeString() }}</td>
                        <td>{{ $service->updated_at->toDayDateTimeString() }}</td>
                        <td>
                            <div class="mt-2">
                                <a class="btn btn-primary" href="{{ url("services/{$service->id}/edit") }}">Edit</a>
                            </div>

                            <div class="mt-2">
                                <a class="btn btn-danger" href="{{ url("services/{$service->id}/confirm-delete") }}">Delete</a>
                            </div>

                            <div class="mt-2">
                                @if($service->active)
                                    <a class="btn btn-warning" href="{{ url("services/{$service->id}/confirm-deactivate") }}">Deactivate</a>
                                @else
                                    <a class="btn btn-success" href="{{ url("services/{$service->id}/confirm-activate") }}">Activate</a>
                                @endif
                            </div>
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-md-12">
                        {{ $services->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('title', 'Add Service')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <h3>Add Service</h3>

                <form method="post" action="{{ url('services') }}">

                    <input type="hidden" value="{{csrf_token()}}" name="_token" />

                    <div class="row">
                        <div class="col-md-6 ml-0">
                            <div class="form-group">
                                <label for="addServiceInputName">Service Name</label>
                                <input type="text" name="name" class="form-control" id="addServiceInputName" aria-describedby="nameHelp" placeholder="Enter service name">
                                <small id="nameHelp" class="form-text text-muted is-invalid">Maximum 100 characters.</small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 ml-0">
                            <div class="form-group">
                                <label for="addServiceInputStatus">Status</label>
                                <select name="active" class="form-control" id="addServiceInputStatus">
                                    <option value="">Select</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Submit</button>

                            <a href="{{ url('services') }}" class="btn btn-secondary ml-2">Cancel</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection
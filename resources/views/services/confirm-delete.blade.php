@extends('layouts.app')

@section('title', 'Confirm Delete Network')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <h3>Confirm Delete Network</h3>

                    <p class="text-danger">
                        This will delete all the associated affiliates, if any.
                    </p>

                <form method="post" action="{{ url("networks/{$network->id}") }}">

                    <input type="hidden" value="{{csrf_token()}}" name="_token" />
                    <input type="hidden" value="DELETE" name="_method" />

                    <div class="row">
                        <div class="col-md-12 mt-3">
                            <button type="submit" class="btn btn-primary">Delete</button>

                            <a href="{{ url('networks') }}" class="btn btn-secondary ml-2">Cancel</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection
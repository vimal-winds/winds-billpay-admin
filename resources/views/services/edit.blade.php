@extends('layouts.app')

@section('title', 'Edit Network')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <h3>Edit Network</h3>

                <form method="post" action="{{ url("networks/{$network->id}") }}">

                    <input type="hidden" value="{{csrf_token()}}" name="_token" />
                    <input type="hidden" value="PUT" name="_method" />

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="addNetworkInputName">Network Name</label>
                                <input type="text" name="name" class="form-control" id="addNetworkInputName"
                                       aria-describedby="nameHelp" placeholder="Enter network name"
                                       value="{{ $network->name }}">
                                <small id="nameHelp" class="form-text text-muted is-invalid">Maximum 100 characters.</small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="addNetworkInputSubidKeyName">Subid Key Name</label>
                                <input type="text" name="subid_key_name" class="form-control" id="addNetworkInputSubidKeyName"
                                       aria-describedby="subidKeyNameHelp" placeholder="Enter subid key name" value="{{ $network->subid_key_name }}">
                                <small id="subidKeyNameHelp" class="form-text text-muted is-invalid">Maximum 50 characters.</small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="addNetworkInputAlternateSubidKeyName">Alternate Subid Key Name</label>
                                <input type="text" name="alternate_subid_key_name" class="form-control" id="addNetworkInputAlternateSubidKeyName"
                                       aria-describedby="alternateSubidKeyNameHelp" placeholder="Enter alternate subid key name" value="{{ $network->alternate_subid_key_name }}">
                                <small id="alternateSubidKeyNameHelp" class="form-text text-muted is-invalid">Maximum 50 characters.</small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Submit</button>

                            <a href="{{ url('networks') }}" class="btn btn-secondary ml-2">Cancel</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('title', 'List Providers')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{Session::get('success')}}
                    </div>
                @endif

                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{Session::get('error')}}
                    </div>
                @endif

                <h3>List Providers</h3>

                <div class="row">
                    <div class="col-md-12">
                        <div class="float-right">
                            <a href="{{ url('providers/create') }}">
                                <button type="button" class="btn btn-primary">Add Provider</button>
                            </a>
                        </div>
                    </div>
                </div>

                <table class="table table-bordered mt-3">
                    <thead>
                    <tr>
                        <th scope="col">Sl. No</th>
                        <th scope="col">Provider ID</th>
                        <th scope="col">Service</th>
                        <th scope="col">Name</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Last Updated At</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($providers as $index => $provider)
                    <tr>
                        <th scope="row">
                            {{ ($providers->currentPage() > 0)
                            ? ((($providers->currentPage()-1) * $providers->perPage()) + $index + 1)
                            : ++$index }}</th>
                        <td>{{ $provider->id }}</td>
                        <td>{{ $provider->services->name }}</td>
                        <td>{{ $provider->name }}</td>
                        <td>{{ $provider->created_at->toDayDateTimeString() }}</td>
                        <td>{{ $provider->updated_at->toDayDateTimeString() }}</td>
                        <td>
                            <div class="mt-2">
                                <a class="btn btn-primary" href="{{ url("providers/{$provider->id}/edit") }}">Edit</a>

                                <a class="btn btn-danger" href="{{ url("providers/{$provider->id}/confirm-delete") }}">Delete</a>

                                @if($provider->active)
                                    <a class="btn btn-warning" href="{{ url("providers/{$provider->id}/confirm-deactivate") }}">Deactivate</a>
                                @else
                                    <a class="btn btn-success" href="{{ url("providers/{$provider->id}/confirm-activate") }}">Activate</a>
                                @endif
                            </div>
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-md-12">
                        {{ $providers->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
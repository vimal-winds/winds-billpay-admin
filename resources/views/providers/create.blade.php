@extends('layouts.app')

@section('title', 'Add Provider')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <h3>Add Provider</h3>

                <form method="post" action="{{ url('providers') }}">

                    <input type="hidden" value="{{csrf_token()}}" name="_token" />

                    <div class="row">
                        <div class="col-md-6 ml-0">
                            <div class="form-group">
                                <label for="addProviderInputName">Provider Name</label>
                                <input type="text" name="name" class="form-control" id="addProviderInputName" aria-describedby="nameHelp" placeholder="Enter provider name">
                                <small id="nameHelp" class="form-text text-muted is-invalid">Maximum 100 characters.</small>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 ml-0">
                            <div class="form-group">
                                <label for="addCheckUrl">Verification Url</label>
                                <input type="text" name="check_url" class="form-control" id="addCheckUrl" aria-describedby="checkUrlHelp" placeholder="Enter Check Url">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 ml-0">
                            <div class="form-group">
                                <label for="addPayUrl">Payment Url</label>
                                <input type="text" name="pay_url" class="form-control" id="addPayUrl" aria-describedby="payUrlHelp" placeholder="Enter Pay Url">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 ml-0">
                            <div class="form-group">
                                <label for="addVerifyUrl">Status Url</label>
                                <input type="text" name="verify_url" class="form-control" id="addVerifyUrl" aria-describedby="verifyUrlHelp" placeholder="Enter Verify Url">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 ml-0">
                            <div class="form-group">
                                <label for="addProviderInputStatus">Status</label>
                                <select name="active" class="form-control" id="addProviderInputStatus">
                                    <option value="">Select</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Submit</button>

                            <a href="{{ url('providers') }}" class="btn btn-secondary ml-2">Cancel</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection
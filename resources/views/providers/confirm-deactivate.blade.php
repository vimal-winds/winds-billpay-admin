@extends('layouts.app')

@section('title', 'Confirm Deactivate Provider')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <h3>Confirm Deactivate Provider</h3>

                    <p>Do you want to deactivate provider <b>{{ $provider->name }}</b>?</p>

                <form method="post" action="{{ url("providers/{$provider->id}/deactivate") }}">

                    <input type="hidden" value="{{csrf_token()}}" name="_token" />
                    <input type="hidden" value="PATCH" name="_method" />

                    <div class="row">
                        <div class="col-md-12 mt-3">
                            <button type="submit" class="btn btn-primary">Deactivate</button>

                            <a href="{{ url('providers') }}" class="btn btn-secondary ml-2">Cancel</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection
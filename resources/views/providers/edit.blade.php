@extends('layouts.app')

@section('title', 'Edit Provider')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <h3>Edit Provider</h3>

                <form method="post" action="{{ url("providers/{$provider->id}") }}">

                    <input type="hidden" value="{{csrf_token()}}" name="_token" />
                    <input type="hidden" value="PUT" name="_method" />

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="addProviderInputName">Provider Name</label>
                                <input type="text" name="name" class="form-control" id="addProviderInputName"
                                       aria-describedby="nameHelp" placeholder="Enter provider name"
                                       value="{{ $provider->name }}">
                                <small id="nameHelp" class="form-text text-muted is-invalid">Maximum 100 characters.</small>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="addServiceInputId">Services</label>
                                <select id="addServiceInputId" name="service_id" class="form-control">
                                    <option>Please Select Service</option>
                                    @foreach($serviceList as $service)
                                        <option value="{{ $service['id'] }}" @if($service['id'] == $provider->service_id) selected="selected" @endif >{{ $service['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Submit</button>

                            <a href="{{ url('providers') }}" class="btn btn-secondary ml-2">Cancel</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection
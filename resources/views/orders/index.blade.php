@extends('layouts.app')

@section('title', 'List Orders')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{Session::get('success')}}
                    </div>
                @endif

                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{Session::get('error')}}
                    </div>
                @endif

                <h3>List Orders</h3>

                <table class="table table-bordered mt-3">
                    <thead>
                    <tr>
                        <th scope="col">Sl. No</th>
                        <th scope="col">Order ID</th>
                        <th scope="col">Provider</th>
                        <th scope="col">User</th>
                        <th scope="col">Number</th>
                        <th scope="col">Total</th>
                        <th scope="col">Status</th>
                        <th scope="col">Payment Method</th>
                        <th scope="col">Payment Ref</th>
                        <th scope="col">Created Date</th>
                        <th scope="col">Last Updated Date</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $index => $order)
                        <tr>
                            <th scope="row">
                                {{ ($orders->currentPage() > 0)
                                ? ((($orders->currentPage()-1) * $orders->perPage()) + $index + 1)
                                : ++$index }}</th>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->providers->name }}</td>
                            <td><!-- {{ $order->user }} --> </td>
                            <td>{{ $order->number }}</td>
                            <td>{{ $order->total }}</td>
                            <td>{{ $order->status }}</td>
                            <td>{{ $order->payment_method }}</td>
                            <td>{{ $order->payment_ref }}</td>
                            <td>{{ $order->created_at->toDayDateTimeString() }}</td>
                            <td>{{ $order->updated_at->toDayDateTimeString() }}</td>
                            <td>
                                <div class="mt-2">
                                    <a class="btn btn-primary" href="{{ url("orders/{$order->id}") }}">View</a>
                                </div>

                                <!-- <div class="mt-2">
                                    <a class="btn btn-primary" href="{{ url("orders/{$order->id}/edit") }}">Edit</a>
                                </div>
                                 -->
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-md-12">
                        {{ $orders->links() }}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@extends('layouts.app')

@section('title', 'View Order')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h3>View Order</h3>

                <div class="form-group">
                    <label class="font-weight-bold">Order Id</label>
                    <div>{{ $order->id }}</div>
                </div>

                <div class="form-group">
                    <label class="font-weight-bold">Provider</label>
                    <div>{{ $order->providers->name }}</div>
                </div>

                <div class="form-group">
                    <label class="font-weight-bold">User</label>
                    <div>{{ $order->user }}</div>
                </div>

                <div class="form-group">
                    <label class="font-weight-bold">Total</label>
                    <div>{{ $order->total }}</div>
                </div>

                <div class="form-group">
                    <label class="font-weight-bold">Status</label>
                    <div>{{ $order->status }}</div>
                </div>

                <div class="form-group">
                    <label class="font-weight-bold">Payment Method</label>
                    <div>
                        {{ $order->payment_method }}
                    </div>
                </div>

                <div class="form-group">
                    <label class="font-weight-bold">Payment Ref</label>
                    <div>{{ $order->payment_ref }}</div>
                </div>

                <a href="{{ url('orders') }}" class="btn btn-primary mb-3">List Orders</a>

            </div>
        </div>
    </div>
@endsection
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Generic error messages
    |--------------------------------------------------------------------------
    */

    'generic_error' => 'Unknown error occurred. Please try again later or contact us if issue persists.',

];

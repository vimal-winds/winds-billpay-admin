<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Provider\ProviderController@index');

// @TODO Group routes and add middleware.

Route::resource('providers', 'Provider\ProviderController');

Route::get('providers/{provider}/confirm-delete', 'Provider\ProviderController@confirmDelete');

Route::get('providers/{provider}/confirm-activate', 'Provider\ProviderController@confirmActivate');

Route::get('providers/{provider}/confirm-deactivate', 'Provider\ProviderController@confirmDeactivate');

Route::patch('providers/{provider}/activate', 'Provider\ProviderController@activate');

Route::patch('providers/{provider}/deactivate', 'Provider\ProviderController@deactivate');

// Service
Route::resource('services', 'Service\ServiceController');

Route::get('services/{service}/confirm-delete', 'Service\ServiceController@confirmDelete');

Route::get('services/{service}/confirm-activate', 'Service\ServiceController@confirmActivate');

Route::get('services/{service}/confirm-deactivate', 'Service\ServiceController@confirmDeactivate');

Route::patch('services/{service}/activate', 'Service\ServiceController@activate');

Route::patch('services/{service}/deactivate', 'Service\ServiceController@deactivate');

// Order
Route::resource('orders', 'Order\OrderController');

<?php

namespace App\Http\Requests;

class EditServiceRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100|unique:' . request()->route()->parameter('service')->id,
            'subid_key_name' => 'required|string|alpha_dash|max:50',
            'alternate_subid_key_name' => 'nullable|alpha_dash|max:50',
        ];
    }
}

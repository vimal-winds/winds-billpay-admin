<?php

namespace App\Http\Controllers;

use Exception;

abstract class ApiController extends Controller
{
    /**
     * Log and return generic internal error.
     *
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    protected function logRespondGenericInternalError(Exception $e)
    {
        if (app_debug_enabled()) {
            throw $e;
        }

        logger()->error($e);

        return $this->respondGenericInternalError();
    }
}

<?php

namespace App\Http\Controllers\Service;

use DB;
use Exception;
use App\Models\Service;
use App\Http\Requests\EditServiceRequest;
use App\Http\Requests\CreateServiceRequest;

class ServiceController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $services = Service::paginate(10);

        return view('services.index', compact('services'));
    }

    /**
     * Show the form for creating a new service.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.create');
    }

    /**
     * Store a newly created service in database.
     *
     * @param CreateServiceRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(CreateServiceRequest $request)
    {
        $name = $request->input('name');
        $active = $request->input('active');

        try {
            Service::create([
                'name' => $name,
                'active' => $active,
            ]);
        } catch (Exception $e) {
            return redirect('/services')->with('error', 'Error occurred trying to add service.');
        }

        return redirect('/services')->with('success', 'Service added successfully!');
    }

    /**
     * Show the form for editing the specified service.
     *
     * @param Service $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Service $service)
    {
        return view('services.edit', compact('service'));
    }

    /**
     * Update the specified service in database.
     *
     * @param EditServiceRequest $request
     * @param Service $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditServiceRequest $request, Service $service)
    {
        $name = $request->input('name');
        $subidKeyName = $request->input('subid_key_name');
        $alternateSubidKeyName = $request->input('alternate_subid_key_name');

        try {
            $service->update([
                'name' => $name,
                'subid_key_name' => $subidKeyName,
                'alternate_subid_key_name' => $alternateSubidKeyName,
            ]);
        } catch (Exception $e) {
            return redirect('/services')->with('error', 'Error occurred trying to update service.');
        }

        return redirect('/services')->with('success', 'Service updated successfully!');
    }

    /**
     * Confirm delete service.
     *
     * @param Service $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirmDelete(Service $service)
    {
        return view('services.confirm-delete', compact('service'));
    }

    /**
     * Confirm activate service.
     *
     * @param Service $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirmActivate(Service $service)
    {
        return view('services.confirm-activate', compact('service'));
    }

    /**
     * Confirm deactivate service.
     *
     * @param Service $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirmDeactivate(Service $service)
    {
        return view('services.confirm-deactivate', compact('service'));
    }

    /**
     * Remove the specified service from database.
     *
     * @param Service $service
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function destroy(Service $service)
    {
        $orderExists = $service->orders()
            ->exists();

        if ($orderExists) {
            return redirect('/services')->with('error', 'Cannot delete service as there are one or more orders for the service.');
        }

        try {
            $service->delete();
        } catch (Exception $e) {
            return redirect('/services')->with('error', 'Error occurred trying to delete the service.');
        }

        return redirect('/services')->with('success', 'Service deleted successfully!');
    }

    /**
     * Remove the specified service from database.
     *
     * @param Service $service
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function activate(Service $service)
    {
        if ($service->isActive()) {
            return redirect('/services')->with('error', 'Service is already active.');
        }

        try {
            $service->update(['active' => true]);
        } catch (Exception $e) {
            return redirect('/services')->with('error', 'Error occurred trying to activate the service.');
        }

        return redirect('/services')->with('success', 'Service activated successfully!');
    }

    /**
     * Remove the specified service from database.
     *
     * @param Service $service
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function deactivate(Service $service)
    {
        if (!$service->isActive()) {
            return redirect('/services')->with('error', 'Service is already inactive.');
        }

        DB::beginTransaction();
        try {

            $service->update(['active' => false]);

            $service->orders()
                ->update(['active' => false]);

        } catch (Exception $e) {
            DB::rollback();
            return redirect('/services')->with('error', 'Error occurred trying to deactivate the service.');
        }
        DB::commit();

        return redirect('/services')->with('success', 'Service Deactivated successfully!');
    }
}

<?php

namespace App\Http\Controllers\Order;

use DB;
use Storage;
use Exception;
use App\Models\Provider;
use App\Models\Order;

class OrderController
{
    /**
     * OrderController constructor.
     *
     */
    public function __construct()
    {
    }

    /**
     * Display listing of orders.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $orders = Order::with('providers')
            ->where('user_id',1)
            ->paginate(10);

        return view('orders.index', compact('orders'));
    }

    /**
     * Show details of the order.
     *
     * @param Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Order $order)
    {
        return view('orders.view', compact('order'));
    }
}

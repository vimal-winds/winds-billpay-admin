<?php

namespace App\Http\Controllers\Provider;

use Exception;
use App\Models\Provider;
use App\Models\Service;
use App\Http\Requests\EditProviderRequest;
use App\Http\Requests\CreateProviderRequest;

class ProviderController
{
    /**
     * Display listing of providers.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $providers = Provider::with('services')->paginate(10);

        return view('providers.index', compact('providers'));
    }

    /**
     * Show the form for creating a new provider.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('providers.create');
    }

    /**
     * Store a new provider.
     *
     * @param CreateProviderRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(CreateProviderRequest $request)
    {
        $name = $request->input('name');
        $check_url = $request->input('check_url');
        $pay_url = $request->input('pay_url');
        $verify_url = $request->input('verify_url');
        $active = $request->input('active');

        try {
            Provider::create([
                'name' => $name,
                'check_url' => $check_url,
                'pay_url' => $pay_url,
                'verify_url' => $verify_url,
                'active' => $active
            ]);
        } catch (Exception $e) {
            return redirect('/providers')->with('error', 'Error trying to add provider.');
        }

        return redirect('/providers')->with('success', 'Provider added successfully!');
    }

    /**
     * Show the form for editing the provider.
     *
     * @param Provider $provider
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Provider $provider)
    {
        $serviceList = Service::all()->toArray();
        $provider->load('services');
        return view('providers.edit', compact('provider','serviceList'));
    }

    /**
     * Update provider.
     *
     * @param EditProviderRequest $request
     * @param Provider $provider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditProviderRequest $request, Provider $provider)
    {
        $name = $request->input('name');
        $service_id = $request->input('service_id');

        try {
            $provider->update(['name' => $name, 'service_id'=>$service_id]);
        } catch (Exception $e) {
            return redirect('/providers')->with('error', 'Error trying to update provider.');
        }

        return redirect('/providers')->with('success', 'Provider updated successfully!');
    }

    /**
     * Display confirm delete page.
     *
     * @param Provider $provider
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirmDelete(Provider $provider)
    {
        return view('providers.confirm-delete', compact('provider'));
    }

    /**
     * Display confirm activate page.
     *
     * @param Provider $provider
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirmActivate(Provider $provider)
    {
        return view('providers.confirm-activate', compact('provider'));
    }

    /**
     * Display confirm deactivate page.
     *
     * @param Provider $provider
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirmDeactivate(Provider $provider)
    {
        return view('providers.confirm-deactivate', compact('provider'));
    }

    /**
     * Delete provider.
     *
     * @param Provider $provider
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function destroy(Provider $provider)
    {
        try {
            $provider->delete();
        } catch (Exception $e) {
            return redirect('/providers')->with('error', 'Error trying to delete provider.');
        }

        return redirect('/providers')->with('success', 'Provider deleted successfully!');
    }

    /**
     * Activate provider.
     *
     * @param Provider $provider
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function activate(Provider $provider)
    {
        if ($provider->isActive()) {
            return redirect('/providers')->with('error', 'Provider is already active.');
        }

        try {
            $provider->update(['active' => true]);
        } catch (Exception $e) {
            return redirect('/providers')->with('error', 'Error trying to activate provider.');
        }

        return redirect('/providers')->with('success', 'Provider activated successfully!');
    }

    /**
     * Deactivate provider.
     *
     * @param Provider $provider
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function deactivate(Provider $provider)
    {
        if (!$provider->isActive()) {
            return redirect('/providers')->with('error', 'Provider is already inactive.');
        }

        try {
            $provider->update(['active' => false]);
        } catch (Exception $e) {
            return redirect('/providers')->with('error', 'Error trying to deactivate provider.');
        }

        return redirect('/providers')->with('success', 'Provider Deactivated successfully!');
    }
}

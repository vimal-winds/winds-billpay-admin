<?php

/**
 * Custom helper functions.
 */

use App\Winds\Libraries\QueryFilter\FilterContract;
use Carbon\Carbon;

if (! function_exists('include_route_files')) {
    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param string $folder
     */
    function include_route_files($folder)
    {
        $path = base_path('routes'. DIRECTORY_SEPARATOR . $folder);
        $rdi = new recursiveDirectoryIterator($path);
        $it = new recursiveIteratorIterator($rdi);

        while ($it->valid()) {
            if (! $it->isDot() && $it->isFile() && $it->isReadable() && $it->current()->getExtension() === 'php') {
                require $it->key();
            }

            $it->next();
        }
    }
}

if (! function_exists('app_debug_enabled')) {
    /**
     * Check if the app debug is enabled.
     *
     * @return bool
     */
    function app_debug_enabled()
    {
        return config('app.debug', false);
    }
}

if (! function_exists('app_debug_disabled')) {
    /**
     * Check if the app debug is disabled.
     *
     * @return bool
     */
    function app_debug_disabled()
    {
        return !app_debug_enabled();
    }
}

if (! function_exists('limit_value')) {
    /**
     * Limit the given input value between the min and max value.
     *
     * @param mixed $value
     * @param mixed $min
     * @param mixed $max
     * @return mixed
     */
    function limit_value($value, $min, $max)
    {
        return min(max($value, $min), $max);
    }
}

if (! function_exists('filter')) {
    /**
     * Get the Query String Filter instance.
     *
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Relations\BelongsToMany|null $builder
     * @param \League\Fractal\TransformerAbstract|callable|null $transformer
     * @param \App\Winds\Libraries\QueryFilter\FilterContract|null $customFilter
     * @return \App\Winds\Libraries\QueryFilter\QueryFilter
     */
    function filter($builder = null, $transformer = null, FilterContract $customFilter = null)
    {
        $queryFilter = app('query-filter');

        if (!is_null($builder)) {
            $queryFilter = $queryFilter->builder($builder);
        }

        if (!is_null($transformer)) {
            $queryFilter = $queryFilter->transformWith($transformer);
        }

        if (!is_null($customFilter)) {
            $queryFilter = $queryFilter->customFilter($customFilter);
        }

        return $queryFilter;
    }
}

if (! function_exists('ip')) {
    /**
     * Get the client IP address.
     *
     * @return string
     */
    function ip()
    {
        return request()->ip();
    }
}

if (! function_exists('is_valid_date')) {
    /**
     * Validate the date time string and return the Carbon instance if needed.
     *
     * @param string $date
     * @param bool $returnDate
     * @return bool|\Carbon\Carbon
     */
    function is_valid_date($date, $returnDate = true)
    {
        if (Validator::make(['date' => $date], ['date' => 'required|date'])->fails()) {
            return false;
        }

        return $returnDate ? Carbon::parse($date) : true;
    }
}

if (! function_exists('resolve_boolean')) {

    /**
     * Resolves a string towards a boolean value.
     *
     * @param $value
     * @return bool
     */
    function resolve_boolean($value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
}

if (! function_exists('file_path')) {
    /**
     * Get the full file path given the folder path and file name.
     *
     * @param string $path
     * @param string $filename
     * @param string $folder The folder inside the path
     * @return string
     */
    function file_path($path, $filename, $folder = null)
    {
        return rtrim($path, '/') . ($folder ? "/{$folder}/" : '/') . $filename;
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Winds\Services\Hash\HashGenerator;
use App\Winds\Services\Hash\HashGeneratorContract;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(HashGeneratorContract::class, HashGenerator::class);
    }
}

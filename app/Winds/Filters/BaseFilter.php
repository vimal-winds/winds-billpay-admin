<?php

namespace App\Winds\Filters;

use Exception;
use App\Winds\Libraries\QueryFilter\FilterContract;

abstract class BaseFilter implements FilterContract
{
    /**
     * Allowed status filter values.
     *
     * @var array
     */
    protected $allowedStatusValues = [];

    /**
     * Apply status filter.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param string $status
     * @throws Exception
     */
    protected function applyStatus($builder, $status)
    {
        if (!in_array($status, $this->allowedStatusValues)) {
            return;
        }

        $method = 'status' . studly_case($status);

        if (!method_exists($this, $method)) {
            throw new Exception("Undefined method '{$method}' in custom filter.");
        }

        $this->$method($builder);
    }

    /**
     * Validates date time string.
     * Returns the Carbon instance of the date if valid.
     *
     * @param string $date
     * @param string $field
     * @return \Carbon\Carbon
     * @throws Exception
     */
    protected function validateDateTime($date, $field = 'date')
    {
        if (($date = is_valid_date($date)) === false) {
            throw new Exception("Invalid {$field}.");
        }

        return $date;
    }

    /**
     * Validates date time string.
     * Returns the Carbon instance of the date (Resets the time to 00:00:00) if valid.
     *
     * @param string $date
     * @param string $field
     * @return \Carbon\Carbon
     * @throws Exception
     */
    protected function validateDate($date, $field = 'date')
    {
        return $this->validateDateTime($date, $field)->startOfDay();
    }

    /**
     * Resolves a string to boolean value.
     *
     * @param string $value
     * @return bool
     */
    protected function resolveBoolean(string $value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }
}

<?php

namespace App\Winds\Libraries\QueryFilter;

interface FilterContract
{
    /**
     * The available filters.
     *
     * @return array
     */
    public function filters();
}

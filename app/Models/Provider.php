<?php

namespace App\Models;

use App\Models\Traits\HasActive;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    use HasActive;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'providers';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean'
    ];

    /**
     * The attributes that can be used for sorting with query string filtering.
     *
     * @var array
     */
    public $sortable = [
        'id', 'name', 'created_at', 'updated_at'
    ];

    /**
     * The list of Service of the provider.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function services()
    {
        return $this->hasOne(Service::class, 'id', 'service_id');
    }
}

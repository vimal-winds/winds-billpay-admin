<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The list of Provider of the Order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function providers()
    {
        return $this->hasOne(Provider::class, 'id', 'provider_id');
    }

    // *
    //  * The list of User of the Order.
    //  *
    //  * @return \Illuminate\Database\Eloquent\Relations\hasOne
     
    // public function users()
    // {
    //     return $this->hasOne(User::class, 'user_id', 'id');
    // }
}

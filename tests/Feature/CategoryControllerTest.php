<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Category;
use App\Models\Affiliate;

class CategoryControllerTest extends TestCase
{
    /**
     * Tests categories are listed correctly.
     *
     * @return void
     */
    public function testsCategoriesAreListedCorrectly()
    {
        $category = factory(Category::class)->create([
            'name' => 'Affiliate Category 1',
            'active' => true,
        ]);

        $response = $this->get('/api/v1/categories');

        $response->assertStatus(200)
            ->assertJson([
                'success' => true,
                'data' => [
                    [
                        'id' => $category->id,
                        'name' => 'Affiliate Category 1',
                        'active' => true,
                        'created_at' => $category->created_at->toDayDateTimeString(),
                        'updated_at' => $category->updated_at->toDayDateTimeString(),
                        'can_delete' => true,
                    ]
                ],
                'meta' => [
                    'pagination' => [
                        'total' => 1,
                        'count' => 1,
                        'per_page' => 10,
                        'current_page' => 1,
                        'total_pages' => 1,
                        'links' => []
                    ]
                ]
            ])
            ->assertJsonStructure([
                'success',
                'data' => [
                    '*' => ['id', 'name', 'active', 'created_at', 'updated_at', 'can_delete']
                ],
                'meta' => [
                    'pagination' => ['total', 'count', 'per_page', 'current_page', 'total_pages', 'links']
                ]
            ]);
    }

    /**
     * Tests category displayed correctly.
     *
     * @return void
     */
    public function testsCategoryDisplayedCorrectly()
    {
        $category = factory(Category::class)->create([
            'name' => 'Affiliate Category 1',
            'active' => true,
        ]);

        $response = $this->get("/api/v1/categories/{$category->id}");

        $response->assertStatus(200)
            ->assertJson([
                'success' => true,
                'data' => [
                    'id' => $category->id,
                    'name' => 'Affiliate Category 1',
                    'active' => true,
                    'created_at' => $category->created_at->toDayDateTimeString(),
                    'updated_at' => $category->updated_at->toDayDateTimeString(),
                    'can_delete' => true,
                ]
            ])
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'active',
                    'created_at',
                    'updated_at',
                    'can_delete',
                ],
            ]);
    }

    /**
     * Tests required fields are validated correctly when categories are created.
     */
    public function testsRequiredFieldsAreValidatedCorrectlyWhenCategoriesAreCreated()
    {
        $payload = [];

        $this->json('POST', '/api/v1/categories', $payload)
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'name' => [
                        0 => 'The name field is required.',
                    ],
                    'active' => [
                        0 => 'The active field is required.',
                    ],
                ]
            ])
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'name',
                    'active',
                ],
            ]);
    }

    /**
     * Tests categories are created correctly.
     */
    public function testsCategoriesAreCreatedCorrectly()
    {
        $payload = [
            'name' => 'Test Category 1',
            'active' => 1,
        ];

        $this->json('POST', '/api/v1/categories', $payload)
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'message' => 'success',
                'data' => [
                    'id' => 1,
                    'name' => 'Test Category 1',
                    'active' => true,
                    'created_at' => now()->toDayDateTimeString(),
                    'updated_at' => now()->toDayDateTimeString(),
                    'can_delete' => true,
                ]
            ])
            ->assertJsonStructure([
                'success',
                'message',
                'data' => [
                    'id',
                    'name',
                    'active',
                    'created_at',
                    'updated_at',
                    'can_delete',
                ],
            ]);
    }

    /**
     * Tests categories are updated correctly.
     */
    public function testsCategoriesAreUpdatedCorrectly()
    {
        $category = factory(Category::class)->create([
            'name' => 'First Category',
            'active' => 1,
        ]);

        $payload = [
            'name' => 'First Category Test',
            'active' => 0,
        ];

        $this->json('PUT', '/api/v1/categories/' . $category->id, $payload)
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'message' => 'success',
                'data' => [
                    'id' => 1,
                    'name' => 'First Category Test',
                    'active' => false,
                    'created_at' => now()->toDayDateTimeString(),
                    'updated_at' => now()->toDayDateTimeString(),
                    'can_delete' => true,
                ]
            ])
            ->assertJsonStructure([
                'success',
                'message',
                'data' => [
                    'id',
                    'name',
                    'active',
                    'created_at',
                    'updated_at',
                    'can_delete',
                ],
            ]);
    }

    /**
     * Tests required fields are validated correctly when categories are updated.
     */
    public function testsRequiredFieldsAreValidatedCorrectlyWhenCategoriesAreUpdated()
    {
        $category = factory(Category::class)->create([
            'name' => 'First Category',
            'active' => 1,
        ]);

        $payload = [];

        $this->json('PUT', '/api/v1/categories/' . $category->id, $payload)
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'name' => [
                        0 => 'The name field is required.',
                    ],
                    'active' => [
                        0 => 'The active field is required.',
                    ],
                ]
            ])
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'name',
                    'active',
                ],
            ]);
    }

    /**
     * Tests categories are deleted correctly.
     */
    public function testsCategoriesAreDeletedCorrectly()
    {
        $category = factory(Category::class)->create([
            'name' => 'First Category Test',
            'active' => 1,
        ]);

        $this->json('DELETE', '/api/v1/categories/' . $category->id)
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'message' => 'success',
            ])
            ->assertJsonStructure([
                'success',
                'message',
            ]);
    }

    /**
     * Tests categories with affiliates are not deleted.
     */
    public function testsCategoriesWithAffiliatesAreNotDeleted()
    {
        $category = factory(Category::class)->create([
            'name' => 'First Category Test',
            'active' => 1,
        ]);

        factory(Affiliate::class)->create([
            'affiliate_category_id' => $category->id,
        ]);

        $this->json('DELETE', '/api/v1/categories/' . $category->id)
            ->assertStatus(500)
            ->assertJson([
                'success' => false,
                'message' => 'This category has affiliate(s), can not delete category.',
                'status_code' => 500,
            ])
            ->assertJsonStructure([
                'success',
                'message',
                'status_code',
            ]);
    }
}
